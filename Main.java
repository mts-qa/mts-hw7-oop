import java.util.ArrayList;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input N:");
        int N = scanner.nextInt();

        CreateAnimalService animals1 = new CreateAnimalService() {};
        CreateAnimalServiceImpl animals2 = new CreateAnimalServiceImpl();
        CreateAnimalServiceImpl animals3 = new CreateAnimalServiceImpl();

        AbstractAnimal[] animals = {
                new Cat("Sphinx","Whiskers",100.0,"Playful", LocalDate.of(2019, 2, 15)),
                new Dog("Labrador","Buddy",150.0,"Friendly", LocalDate.of(2018, 7, 10)),
                new Shark("Great White", "Jaws", 500.0,"Aggressive", LocalDate.of(2020, 5, 20)),
                new Wolf("Grey Wolf", "Alpha", 200.0,"Pack leader", LocalDate.of(2017, 11, 30)),
                new Cat("Sphinx","Whiskers",100.0,"Playful", LocalDate.of(2019, 2, 15)),
                new Dog("Labrador","Buddy",150.0,"Friendly", LocalDate.of(2008, 7, 10)),
                new Dog("Labrador","Sigma",150.0,"Friendly", LocalDate.of(2018, 7, 10)),
                new Shark("Great White", "Jaws", 500.0,"Aggressive", LocalDate.of(2014, 5, 20)),
                new Wolf("Grey Wolf", "Beta", 200.0,"Pack leader", LocalDate.of(2015, 11, 30)),
                new Dog("Border Collie","Vega",150.0,"Friendly", LocalDate.of(2018, 7, 10)),
        };

        System.out.println("### Часть 1 ###");
        System.out.println("Первый метод (default)");
        animals1.getAnimal(animals);
        System.out.println("\nВторой метод (override)");
        animals2.getAnimal(animals);
        System.out.println("\nТретий метод (overloaded)");
        animals3.getAnimal(animals, N);

        SearchServiceImpl searchService = new SearchServiceImpl();

        System.out.println("\n### Часть 2 ###");
        System.out.println("\nAnimals born in a leap year: ");
        searchService.findLeapYearNames(animals);

        System.out.println("\nAnimals older than " + N + " years:");
        searchService.findOlderAnimal(animals, N);

        System.out.println("\nDuplicates: ");
        searchService.findDuplicate(animals);


    }
}
