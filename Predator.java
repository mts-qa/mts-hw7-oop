import java.time.LocalDate;

public class Predator extends AbstractAnimal {
    public Predator(String breed, String name, Double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }

    @Override
    public void getBreed() {

    }

    @Override
    public void getName() {

    }

    @Override
    public void getCost() {

    }

    @Override
    public void getCharacter() {

    }

    @Override
    public void getBirthDate() {

    }

    @Override
    public void equals() {

    }
}