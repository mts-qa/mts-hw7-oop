import java.util.ArrayList;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.stream.Stream;

public class SearchServiceImpl implements SearchService {
    @Override
    public void findLeapYearNames(AbstractAnimal[] animals) {
        ArrayList<String> leapYearAnimals = new ArrayList<>();
        // With Stream API:
        Stream.of(animals)
                .filter(x->x.birthDate.isLeapYear())
                .map(x-> Map.of(x.getClass().getSimpleName(),List.of(x.name, x.birthDate)))
                .forEach(System.out::println);
    }
    // With for loop:
//        for (AbstractAnimal animal : animals) {
//
//
//            if (animal.birthDate.getYear() % 4 == 0) { // && animal.birthDate.isLeapYear()) {
//                leapYearAnimals.add(animal.name);
//            }
//        }
//        return leapYearAnimals;

    @Override
    public void findOlderAnimal(AbstractAnimal[] animals, int N) {
        HashMap<String, List> OlderAnimals = new HashMap<>();
        for (AbstractAnimal animal: animals) {
            if (LocalDate.now().getYear() - animal.birthDate.getYear() > N) {
                System.out.println(Map.of(animal.getClass().getSimpleName(),
                        List.of(animal.breed, animal.name, animal.cost, animal.character, animal.birthDate)));
            }
        }
//        return OlderAnimals;
    }
    @Override
    public void findDuplicate(AbstractAnimal[] animals) {
        HashMap<List, Integer> mapOfAnimals = new HashMap<>();
        for (AbstractAnimal animal: animals) {
            List<String> listOfAnimal = List.of(animal.getClass().getSimpleName(),
                    animal.breed, animal.name,  String.valueOf(animal.cost), animal.character, String.valueOf(animal.birthDate));
            if (mapOfAnimals.containsKey(listOfAnimal)) {
                mapOfAnimals.replace(listOfAnimal, mapOfAnimals.get(listOfAnimal), mapOfAnimals.get(listOfAnimal)+1);
            }
            else {
                mapOfAnimals.put(listOfAnimal, 1);
            }
        }
        for (List animal: mapOfAnimals.keySet()) {
            if (mapOfAnimals.get(animal) > 1) {
                System.out.println(animal);
            }
        }
//        return Duplicates;
    }
}
