import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface SearchService {
    void findLeapYearNames(AbstractAnimal[] animals);
    void findOlderAnimal(AbstractAnimal[] animals, int N);
    void findDuplicate(AbstractAnimal[] animals);
}
