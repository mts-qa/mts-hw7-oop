public class CreateAnimalServiceImpl implements CreateAnimalService {
    @Override
    public void getAnimal(AbstractAnimal[] animals) {
        int i = 0;
        do {
            System.out.println("Animal name is " + animals[i].name);
            i++;
        }
        while (i < 10);
    }

    public void getAnimal(AbstractAnimal[] animals, int N) {
        for (int i = 0; i<N; i++) {
            System.out.println("Animal name is " + animals[i].name);
        }
    }
}

