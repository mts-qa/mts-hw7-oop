import java.time.LocalDate;

public class Pet extends AbstractAnimal {
    public Pet(String breed, String name, Double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }

    @Override
    public void getBreed() {

    }

    @Override
    public void getName() {

    }

    @Override
    public void getCost() {

    }

    @Override
    public void getCharacter() {

    }

    @Override
    public void getBirthDate() {

    }

    @Override
    public void equals() {

    }
}